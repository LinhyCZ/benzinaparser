package parser;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import java.io.*;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.Scanner;

import java.util.Date;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.io.FileOutputStream;
import java.io.InputStream;
import javax.mail.search.AndTerm;
import javax.mail.search.ComparisonTerm;
import javax.mail.search.FlagTerm;
import javax.mail.search.FromTerm;
import javax.mail.search.ReceivedDateTerm;

public class MainParser {
	static final String username = "benzina@linhy.cz";
	static final String password = "zE2@oz4DDx";
	static final String imapHost = "linhy.cz";
	static final int imapPort = 143;
	static final String BENZINA_EMAIL_ADDRESS = "noreply@benzinaorlen.cz";
	static final String TARGET_RESULT_EMAIL_ADDRESS = "lin.pavel@seznam.cz,tomaslinhys@gmail.com";

	public static void main(String[] args) throws IOException {
		if (!deleteFiles()) {
			System.out.println("Failed to delete previous files, stopping..");
			return;
		}
		downloadEmails();
		String result = processFiles();
		if (!result.isEmpty()) {
			sendEmail(username, password, TARGET_RESULT_EMAIL_ADDRESS, "Tankování " + getMonthName() + " " + LocalDate.now().getYear(), result);
		}

		System.out.println("All is done!");
	}

	public static String getMonthName() {
		// Get the current date
		LocalDate currentDate = LocalDate.now();

		// Get the date of the previous month
		LocalDate previousMonthDate = currentDate.minusMonths(1);

		// Get the name of the previous month in Czech
		return previousMonthDate.getMonth().getDisplayName(TextStyle.FULL_STANDALONE, new Locale("cs"));
	}

	public static boolean deleteFiles() {
		// Get the current directory
		String currentDir = System.getProperty("user.dir");

		// Create a File object for the current directory
		File directory = new File(currentDir);

		// List all files in the directory
		File[] files = directory.listFiles();

		if (files != null) {
			// Iterate through each file
			for (File file : files) {
				// Check if the file is a CSV file
				if (file.isFile() && file.getName().toLowerCase().endsWith(".csv")) {
					// Attempt to delete the file
					boolean deleted = file.delete();
					if (!deleted) {
						return false;
					}
				}
			}
		}

		return true;
	}
	public static void downloadEmails() {
		Properties props = new Properties();
		props.put("mail.imap.host", imapHost);
		props.put("mail.imap.port", imapPort);
		props.put("mail.imap.ssl.enable", "false");

		try {
			Session session = Session.getDefaultInstance(props);
			Store store = session.getStore("imap");
			store.connect(username, password);

			Folder inbox = store.getFolder("INBOX");
			inbox.open(Folder.READ_WRITE);

			Date twoWeeksAgo = new Date(System.currentTimeMillis() - (14 * 24 * 60 * 60 * 1000)); // Two weeks ago

			Message[] messages = inbox.search(
					new AndTerm(
							new AndTerm(
									new ReceivedDateTerm(ComparisonTerm.GT, twoWeeksAgo),
									new FromTerm(new InternetAddress(BENZINA_EMAIL_ADDRESS))
							),
							new FlagTerm(new Flags(Flags.Flag.SEEN), false)
					)
			);
			for (int i = 0; i < Math.min(messages.length, 2); i++) {
				Message message = messages[i];
				if (message.getReceivedDate().after(twoWeeksAgo)) {
					Object content = message.getContent();
					if (content instanceof Multipart) {
						Multipart multipart = (Multipart) content;
						for (int j = 0; j < multipart.getCount(); j++) {
							BodyPart bodyPart = multipart.getBodyPart(j);
							if (Part.ATTACHMENT.equalsIgnoreCase(bodyPart.getDisposition())) {
								InputStream is = bodyPart.getInputStream();
								FileOutputStream fos = new FileOutputStream(bodyPart.getFileName());
								byte[] buf = new byte[4096];
								int bytesRead;
								while ((bytesRead = is.read(buf)) != -1) {
									fos.write(buf, 0, bytesRead);
								}
								fos.close();
								is.close();
							}
						}
					}
				}

				// Mark the message as read so we do not process it again
				message.setFlag(Flags.Flag.SEEN, true);
			}

			inbox.close(false);
			store.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sendEmail(String username, String password, String to, String subject, String content) {
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "false");
		props.put("mail.smtp.host", "linhy.cz");
		props.put("mail.smtp.port", "587");
		props.put("mail.mime.charset", "UTF-8");

		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(username, password);
			}
		});

		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(username));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
			message.setSubject(subject);
			message.setText(content);
			Transport.send(message);
			System.out.println("Email sent successfully!");
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Main method to process *.csv files that are already present in the working directory
	 * @throws IOException
	 */
	public static String processFiles() throws IOException {
		Gson gson = new Gson();
		InputStream is = new FileInputStream(new File("cards.json"));
		JsonReader reader = new JsonReader(new InputStreamReader(is, StandardCharsets.UTF_8));
		
		Type type = new TypeToken<HashMap<String, String>>(){}.getType();
		Map<String, String> karty = gson.fromJson(reader, type);
		
		File dir = new File(".");
		File[] files = dir.listFiles();

		StringBuilder result = new StringBuilder();
		
		for (int i = 0; i < files.length; i++) {
			if (!files[i].getName().equals("Run.cmd") && !files[i].getName().equals("cards.json") && !files[i].getName().equals("parser") && files[i].getName().toLowerCase().contains(".csv")) {
				Scanner sc = new Scanner(files[i], Charset.forName("cp1250"));
								
				Map<String, Double> ceny = new HashMap<>();
				Map<String, Double> litry = new HashMap<>();
				Map<String, String> tankovani = new HashMap<>();
				
				boolean skip = true;

				while (sc.hasNextLine()) {
					String[] data = sc.nextLine().split(";");
					if (skip) {
						skip = false;
						continue;
					}

					String cisloKarty = data[3].substring(2, data[3].length() - 1);
					Double mnozstvi = Double.parseDouble(data[7].replace(',', '.').substring(1, data[7].length() - 1));
					Double cena = Double.parseDouble(data[8].replace(',', '.').substring(1, data[8].length() - 1));
					
					String text = data[4].substring(1, data[4].length() - 1) + " natankováno " + data[7].replace(',', '.').substring(1, data[7].length() - 1) + "l " + data[6].substring(1, data[6].length() - 1) + " za " + data[8].replace(',', '.').substring(1, data[8].length() - 1) + " Kč na čerpací stanici " + data[10].substring(1, data[10].length() - 1) + "\n";
					if (tankovani.get(cisloKarty) == null) {
						tankovani.put(cisloKarty, text);
					} else {
						tankovani.put(cisloKarty, tankovani.get(cisloKarty) + text);
					}
					
					
					Double staraCena = ceny.get(cisloKarty);
					Double stareMnozstvi = litry.get(cisloKarty);
					
					if (staraCena == null) staraCena = 0.0;
					if (stareMnozstvi == null) stareMnozstvi = 0.0;

					ceny.put(cisloKarty, staraCena + cena);
					litry.put(cisloKarty, stareMnozstvi + mnozstvi);
				}

                for (String cisloKarty : karty.keySet()) {
                    if (litry.get(cisloKarty) == null) continue;
                    Double mnozstvi = Math.round(litry.get(cisloKarty) * 100) / 100.0;
                    Double cena = Math.round(ceny.get(cisloKarty) * 100) / 100.0;

                    result.append(karty.get(cisloKarty)).append(" natankováno celkem: ").append(mnozstvi).append("l, celková cena: ").append(cena).append("Kč").append("\n");
                    result.append(tankovani.get(cisloKarty)).append("\n");
                }
			}
		}

		return result.toString();
	}
}
